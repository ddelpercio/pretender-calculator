import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Path} from './path';

@Component({
  selector: 'app-paths',
  templateUrl: './paths.component.html',
  styleUrls: ['./paths.component.css']
})
export class PathsComponent implements OnInit {

  @Input() pPoints: number;
  @Output() pPointsChange = new EventEmitter<number>();
  paths: Path[];
  constructor() {

    this.paths = [
      {id: 1, name: 'fire', value: 0, shorthand: 'F'},
      {id: 2, name: 'air', value: 0, shorthand: 'A'},
      {id: 3, name: 'water', value: 0, shorthand: 'W'},
      {id: 4, name: 'Earth', value: 0, shorthand: 'E'},
      {id: 5, name: 'Astral', value: 0, shorthand: 'S'},
      {id: 6, name: 'death', value: 0, shorthand: 'D'},
      {id: 7, name: 'nature', value: 0, shorthand: 'N'},
      {id: 8, name: 'blood', value: 0, shorthand: 'B'}
    ];
  }




  ngOnInit() {

  }

  onChange(changedId, changedValue): void {
    this.paths.find(x => x.id === changedId).value = changedValue;
    this.pPoints = this.calculatePointCost(this.paths);
    this.pPointsChange.emit(this.pPoints);
  }


  private calculatePointCost(paths: Path[]) {
    var cost = 0;
    for (let path of paths) {
      cost = cost + this.calcPathCost(path.value, 0);
    }
    console.log('Point Cost', cost);
    return cost;
  }

  private calcPathCost(value, number: number) {
    var difference;
    difference = value - number;
    var cost = 0;
    for (var i = 0 ; i < difference ; i++) {
      console.log('Path Cost', cost);
      cost = cost + ((i + 1) * 8);
    }
    console.log('Path Cost', cost);
    return cost;
  }
}
