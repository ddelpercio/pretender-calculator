export class Path {
  id: number;
  name: string;
  value: number;
  shorthand: string;
}
