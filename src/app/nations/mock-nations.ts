import { Nation } from './nation';

export var NATIONS: Nation[] = [
  {id: 11, name: 'Arco'},
  {id: 12, name: 'Ermor'},
  {id: 13, name: 'Pythium'},
  {id: 14, name: 'Sceleria'},
  {id: 15, name: 'Xibalba'},
  {id: 16, name: 'T\'ien Chi'},
  {id: 17, name: 'Eriu'},
  {id: 18, name: 'Man'},
  {id: 19, name: 'Mictlan'},
  {id: 20, name: 'Atlantis'}
];
