import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PretendersComponent } from './pretenders.component';

describe('PretendersComponent', () => {
  let component: PretendersComponent;
  let fixture: ComponentFixture<PretendersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PretendersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PretendersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
