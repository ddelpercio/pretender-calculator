import {Component, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {Input} from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnChanges {
  title = 'Pretender Calc';
  startingPoints: number;
  pretenderPoints: number;
  pathPoints: number;
  scalePoints: number;
  imprisonmentPoints: number;

  constructor() {
    this.startingPoints = 425;
    this.pretenderPoints = 425;
    this.pathPoints = 0;
    this.imprisonmentPoints = 0;
    this.scalePoints = 0;
}

  ngOnChanges(changes: SimpleChanges) {
    this.pretenderPoints = this.startingPoints +
      this.imprisonmentPoints -
      this.scalePoints - this.pathPoints;
  }

}
