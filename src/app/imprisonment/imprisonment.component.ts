import {Component, EventEmitter, Input, OnChanges, Output, SimpleChanges} from '@angular/core';

@Component({
  selector: 'app-imprisonment',
  templateUrl: './imprisonment.component.html',
  styleUrls: ['./imprisonment.component.css']
})
export class ImprisonmentComponent implements OnChanges {
  @Input() impPoints: number;
  @Output() impPointsChange = new EventEmitter<number>();
  logs: String[] = [];


  constructor() {

  }
ngOnChanges(changes: SimpleChanges) {
this.impPointsChange.emit(this.impPoints);

  }

}
