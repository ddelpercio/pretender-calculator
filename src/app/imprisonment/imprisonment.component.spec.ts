import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ImprisonmentComponent } from './imprisonment.component';

describe('ImprisonmentComponent', () => {
  let component: ImprisonmentComponent;
  let fixture: ComponentFixture<ImprisonmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ImprisonmentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ImprisonmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
