import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-dominion',
  templateUrl: './dominion.component.html',
  styleUrls: ['./dominion.component.css']
})
export class DominionComponent implements OnInit {

  @Input() sPoints: number;
  @Output() sPointsChange = new EventEmitter<number>();
  dominion: number;
  order: number;
  production: number;
  heat: number;
  growth: number;
  fortune: number;
  magic: number;
  constructor() {
    this.sPoints = 0;
    this.dominion = 1;
    this.order = 0;
    this.production = 0;
    this.heat = 0;
    this.growth = 0;
    this.fortune = 0;
    this.magic = 0;
  }

  ngOnInit() {
  }


  onChange(changedValue, whatChanged): void {
    changedValue = parseInt(changedValue, 10);

    switch (whatChanged) {
      case 'dominion': { this.dominion = changedValue; break; }
      case 'order' : { this.order = changedValue; break; }
      case 'production' : { this.production = changedValue; break; }
      case 'heat' : { this.heat = changedValue; break; }
      case 'growth' : { this.growth = changedValue; break; }
      case 'fortune' : { this.fortune = changedValue; break; }
      case 'magic' : { this.magic = changedValue; break; }
    }
    this.sPoints = 40 * (this.order + this.production + this.heat + this.growth + this.fortune + this.magic );
    this.sPoints = this.sPoints + this.calcDominionCost(this.dominion, 1);
    this.sPointsChange.emit(this.sPoints);
  }

  private calcDominionCost(dominion: number, number: number) {
    var cost = 0;
    for (var i = 0; i < dominion; i++) {
      var costOfPath = i + 1 - number;
      if (costOfPath < 0) { costOfPath = 0; }
      cost = cost + (( costOfPath) * 7);
    }
    return cost;
  }
}
