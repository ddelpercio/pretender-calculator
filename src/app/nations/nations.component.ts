import { Component, OnInit } from '@angular/core';


import { Nation } from './nation';
import { NationService } from './nations.service';

@Component({
  selector: 'app-nations-list',
  templateUrl: './nations.component.html',
  styleUrls: [ './nations.component.css' ]
})
export class NationsComponent implements OnInit {
  nations: Nation[];
  selectedNation: Nation;
  defaultNation: Nation;

  constructor(

    private nationService: NationService) { }

  getNations(): void {
    this.nationService.getNations().then(nations => this.nations = nations);
  }

  ngOnInit(): void {
    this.getNations();
    this.defaultNation = {id: 0, name: 'Any'};
    this.selectedNation = this.defaultNation;
  }

  onSelect(nation: Nation): void {
    this.selectedNation = nation;
  }

  gotoDetail(): void {
  //  this.router.navigate(['/detail', this.selectedNation.id]);
  }
}
