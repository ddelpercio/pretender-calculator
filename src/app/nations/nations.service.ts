import { Nation } from './nation';
import { NATIONS } from './mock-nations';
import { Injectable } from '@angular/core';

@Injectable()
export class NationService {
  getNations(): Promise<Nation[]> {
    return Promise.resolve(NATIONS);
  }

  getNationsSlowly(): Promise<Nation[]> {
    return new Promise(resolve => {
      // Simulate server latency with 2 second delay
      setTimeout(() => resolve(this.getNations()), 2000);
    });
  }

  getNation(id: number): Promise<Nation> {
    return this.getNations()
      .then(nations => nations.find(nation => nation.id === id));
  }
}
