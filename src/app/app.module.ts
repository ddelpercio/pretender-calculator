import { BrowserModule } from '@angular/platform-browser';
import {FormsModule} from "@angular/forms";
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { NationsComponent } from './nations/nations.component';
import { NationService } from './nations/nations.service';
import { PathsComponent } from './paths/paths.component';
import { DominionComponent } from './dominion/dominion.component';
import { ImprisonmentComponent } from './imprisonment/imprisonment.component';
import { PretendersComponent } from './pretenders/pretenders.component';


@NgModule({
  declarations: [
    AppComponent,
    NationsComponent,
    PathsComponent,
    DominionComponent,
    ImprisonmentComponent,
    PretendersComponent
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [NationService],
  bootstrap: [AppComponent]
})
export class AppModule { }
